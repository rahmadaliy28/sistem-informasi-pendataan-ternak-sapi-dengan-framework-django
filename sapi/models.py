from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse

# Create your models here.
class Peternakan(models.Model):
    KATEGORI_CHOICES = (
        ('sppr','Sapi Perah'),
        ('sppt','Sapi Potong'),
        ('sptr','Sapi Ternak'),
    )
    nama_sapi = models.CharField('Nama Sapi', max_length=50, null=False)
    kategori = models.CharField(max_length=4, choices=KATEGORI_CHOICES)
    keterangan = models.TextField()
    berat = models.FloatField('Berat (kg)')
    harga = models.FloatField('Harga (Rp)')
    jumlah = models.IntegerField()
    tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-tgl_input']

    def __str__(self):
        return self.nama_sapi

    def get_absolute_url(self): 
        return reverse('home_page')
