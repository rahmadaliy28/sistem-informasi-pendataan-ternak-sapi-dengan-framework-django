from django.contrib import admin
from .models import Peternakan

# Register your models here.
@admin.register(Peternakan)
class PeternakanAdmin(admin.ModelAdmin):
    list_display = ['nama_sapi','kategori','harga','berat',
                    'keterangan','tgl_input','user']
    list_filter = ['nama_sapi','kategori','harga','user']
    search_fields = ['nama_sapi','kategori','user']
